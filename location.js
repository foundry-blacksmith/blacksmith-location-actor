import {LOCATION_CONFIG} from "./scripts/config.js";
import {Location} from "./scripts/location-entity.js";
import {Locations} from "./scripts/locations.js";
import {LocationDirectory} from "./scripts/location-directory.js";
import {LocationSheet} from "./scripts/location-sheet.js";

const logger = LOCATION_CONFIG.logger;
const debug = LOCATION_CONFIG.debug;
const zombie = LOCATION_CONFIG.zombie;

function _onInit() {
    console.group('location.init');
    logger(`Initializing Location Entity module.\n${LOCATION_CONFIG.ASCII}`);

    // Integrate Location set of classes with the rest of the system.
    CONFIG.LOCATION = LOCATION_CONFIG;

    ENTITY_TYPES.push("Location");
    ENTITY_LINK_TYPES.push("Location");
    FOLDER_ENTITY_TYPES.push("Location");
    COMPENDIUM_ENTITY_TYPES.push("Location");
    MACRO_SCOPES.push("locations", "location");

    CONFIG["Location"] = {
        entityClass: Location,
        collection: Locations,
        sheetClasses: {"base": {}},
        sidebarIcon: "fas fa-landmark"
    };
    CONFIG["ui"]["locations"] = LocationDirectory;

    USER_PERMISSIONS["CREATE_LOCATION"] = {
        label: "LOCATION.Create",
        hint: "LOCATION.CreateHint",
        disableGM: false,
        defaultRole: USER_ROLES.ASSISTANT
    };
    debug({CONFIG, ENTITY_TYPES, ENTITY_LINK_TYPES, FOLDER_ENTITY_TYPES, COMPENDIUM_ENTITY_TYPES, MACRO_SCOPES, USER_PERMISSIONS});
    console.groupEnd();
}

function _onSetup() {
    console.group('location.setup');
    logger(`Setting up Location Entity module.`);
    if (!(Location in game.system.entityTypes)) {
        debug('Adding Location to game.system.entitytypes');
        game.system.entityTypes["Location"] = ["base"];
    }
    if (!(Location in game.system.entityTypes.Folder)) {
        debug('Adding Location to Folder');
        game.system.entityTypes.Folder.push(Location);
    }
    if (!("locations" in game.data)) {
        debug('Initializing empty list to game.data.locations');
        game.data.locations = [];
    }
    if (!("locations" in game)) {
        debug('Instantiating Locations and assigning to game.locations');
        game.locations = new Locations(game.data.locations);
    }
    console.groupEnd();
}

async function _onReady() {
    console.group('location.ready');
    logger(`Game is ready, loading existing locations to the game.`);
    debug('Calling render on ui.locations');
    ui.locations.render(true);

    Locations.registerSheet("core", LocationSheet, {types: ["base"], makeDefault: true});

    debug(`ui.locations = `, zombie(ui.locations));
    await _injectInSidebar();
    console.groupEnd();
}

async function _injectInSidebar() {
    let sidebar = null;
    ui.sidebar.render(true);
    sidebar = ui.sidebar.element[0];
    const nav = sidebar.querySelector('#sidebar-tabs');
    const actorsTab = nav.querySelector('[data-tab="actors"]');
    const locationsTab = actorsTab.cloneNode(true);
    const locAttrs = locationsTab.attributes;

    for (let i = 0; i < locAttrs.length; i++) {
         locAttrs[i].value = String(locAttrs[i].value)
             .replace(/actor/g, "location")
             .replace(/Actor/g, "Location");
    }

    let content = locationsTab.firstChild;
    content.setAttribute('class', CONFIG.Location.sidebarIcon);
    actorsTab.before(locationsTab);

    let html = await renderTemplate(ui.locations.template, ui.locations.getData());
    debug(html)
    let locDir = $.parseHTML(html.trim())[0];
    debug({locDir});
    //ui.locations.activateListeners(locDir);
    sidebar.append(locDir);

    debug({sidebar});
}


Hooks.once("init", _onInit);
Hooks.once("setup", _onSetup);
Hooks.once("ready", _onReady);
